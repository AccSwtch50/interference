import os
import time
import json
import random
from requests import get
from requests import post
import openai
from anthropic import Anthropic, HUMAN_PROMPT, AI_PROMPT
import noverif
import conv_module

#from g4f import Models, ChatCompletion, Providers
from flask import Flask, request, Response, jsonify
from flask_cors import CORS
from werkzeug.exceptions import abort

app = Flask(__name__)
CORS(app)

anthropic = Anthropic()

config_file_path = "config.json"

with open(config_file_path, "r") as config_file:
    config_data = json.load(config_file)

models_data = []
api_url = ''
api_key = ''

def add_model(id, object, owned_by, tokens, type, permission, category, real_chat, friendly_name, local_id=None, streaming=True, chatbot_ui_visible=True):
    model = {
        "id": id,
        "object": object,
        "owned_by": owned_by,
        "tokens": tokens,
        "type": type,
        "permission": permission,
        "category": category,
        "real_chat": real_chat,
        "streaming": streaming,
        "chatbot_ui_settings": {
            "friendly_name": friendly_name,
            "local_id": local_id,
            "visible": chatbot_ui_visible
        }
    }
    models_data.append(model)
    
add_model("gpt-3.5-turbo", "model", "openai", 4000, "chat", [], "chatgpt_3.5", True, "GPT-3.5", "GPT_3_5")
add_model("gpt-3.5-turbo-0301", "model", "openai", 4000, "chat", [], "chatgpt_3.5_old", True, "GPT-3.5-0301", "GPT_3_5_0301")
add_model("gpt-3.5-turbo-0613", "model", "openai", 4000, "chat", [], "chatgpt_3.5", True, "GPT-3.5-0613", "GPT_3_5_0613")
add_model("gpt-3.5-turbo-16k", "model", "openai", 16000, "chat", [], "chatgpt_3.5", True, "GPT-3.5-16K", "GPT_3_5_16K")
add_model("gpt-3.5-turbo-16k-0613", "model", "openai", 16000, "chat", [], "chatgpt_3.5", True, "GPT-3.5-16K-0613", "GPT_3_5_16K_0613")
add_model("gpt-4", "model", "openai", 8000, "chat", [], "gpt_4", True, "GPT-4")
#add_model("gpt-4-web", "model", "shadowwrite", 8000, "chat", [], "shadowgpt", True, "GPT-4-WEB")
add_model("gpt-4-0613", "model", "openai", 8000, "chat", [], "gpt_4", True, "GPT-4-0613")
add_model("gpt-4-0314", "model", "openai", 8000, "chat", [], "gpt_4_old", True, "GPT-4-0314")
add_model("gpt-4-32k", "model", "openai", 32000, "chat", [], "gpt_4_32k", True, "GPT-4-32K")
add_model("gpt-4-32k-0613", "model", "openai", 32000, "chat", [], "gpt_4_32k", True, "GPT-4-32K-0613")
add_model("gpt-4-32k-0314", "model", "openai", 32000, "chat", [], "gpt_4_32k_old", True, "GPT-4-32K-0314")
add_model("text-davinci-003", "model", "openai", 2000, "chat", [], "gpt_3.5_td3", False, "text-davinci-003 (GPT-3.5)")
add_model("text-davinci-002", "model", "openai", 2000, "chat", [], "gpt_3.5", False, "text-davinci-002 (GPT-3.5)", streaming=False)
add_model("text-davinci-001", "model", "openai", 1000, "chat", [], "gpt_3", False, "text-davinci-001 (GPT-3)", streaming=False)
add_model("davinci-instruct-beta", "model", "openai", 1000, "chat", [], "gpt_3", False, "Davinci InstructGPT Beta (GPT-3)", streaming=False)
add_model("davinci", "model", "openai", 1000, "chat", [], "gpt_3", False, "Davinci (GPT-3)", streaming=False)
add_model("claude-1", "model", "anthropic", 100000, "chat", [], "claude_obscure", True, "Claude 1")
add_model("claude-instant-1", "model", "anthropic", 100000, "chat", [], "claude", True, "Claude Instant")
add_model("claude-2", "model", "anthropic", 100000, "chat", [], "claude", True, "Claude 2")
add_model("gpt-3.5-turbo-poe", "model", "poe", 2100, "chat", [], "chimera", True, "GPT-3.5 Poe", "GPT_3_5_POE")
add_model("gpt-3.5-turbo-16k-poe", "model", "poe", 16000, "chat", [], "chimera", True, "GPT-3.5-16K Poe", "GPT_3_5_16K_POE")
add_model("gpt-4-poe", "model", "poe", 2800, "chat", [], "chimera", True, "GPT-4 Poe")
add_model("gpt-4-32k-poe", "model", "poe", 32000, "chat", [], "chimera", True, "GPT-4-32K Poe")
add_model("sage", "model", "poe", 5200, "chat", [], "chimera", True, "Sage")
add_model("claude-instant-poe", "model", "poe", 11000, "chat", [], "chimera", True, "Claude Instant Poe")
add_model("claude-instant-100k-poe", "model", "poe", 100000, "chat", [], "chimera", True, "Claude Instant 100k Poe")
add_model("claude-2-100k-poe", "model", "poe", 100000, "chat", [], "chimera", True, "Claude 2 100k Poe")
#add_model("claude+", "model", "poe", 11000, "chat", [], "chimera", True, "Claude+", "CLAUDE_PLUS")
add_model("bard", "model", "google", 10000, "chat", [], "webraft", True, "Google Bard")
add_model("chat-bison-001", "model", "poe", 10000, "chat", [], "webraft", True, "PaLM 2", "PALM2")
add_model("palm-2", "model", "poe", 10000, "chat", [], "webraft", True, "PaLM 2", "PALM2", chatbot_ui_visible=False)
add_model("bing-purgpt", "model", "microsoft", 8000, "chat", [], "conv", True, "Bing (PurGPT)")
add_model("llama-13b", "model", "h2o", 2000, "chat", [], "cyclone", True, "LLaMa 13B", "LLAMA_13B")
add_model("falcon-7b", "model", "h2o", 2000, "chat", [], "cyclone", True, "Falcon 7B")
add_model("falcon-40b", "model", "h2o", 2000, "chat", [], "cyclone", True, "Falcon 40B")
add_model("mpt-30b", "model", "huggingface", 2000, "chat", [], "cyclone", True, "MPT 30B")
add_model("chatglm2", "model", "huggingface", 2000, "chat", [], "cyclone", True, "ChatGLM 2")
add_model("bing", "model", "microsoft", 10000, "chat", [], "cyclone", True, "Bing")
add_model("starchat", "model", "huggingface", 2000, "chat", [], "cyclone", True, "StarChat")
add_model("llama-2-70b-chat", "model", "huggingface", 2000, "chat", [], "cyclone", True, "LLaMa 2 70B")
add_model("oasst-sft-6-llama-30b", "model", "huggingface", 2000, "chat", [], "chimera", True, "Open Assistant (LLaMa 30B SFT-6)")
add_model("claude-2-cyclone", "model", "anthropic", 100000, "chat", [], "cyclone", True, "Claude 2 (CycloneGPT)")
add_model("pai-001-beta", "model", "pawankrd", 8000, "chat", [], "pawanai", True, "Pawan AI")
add_model("pai-001-light-beta", "model", "pawankrd", 4000, "chat", [], "pawanai_light", True, "Pawan AI Light")

dummy_key = 'sk-1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKL'
api_info = {
    'skailar': {
        'key': os.environ['SKAILAR_KEY'],
        'url': 'https://api.skailar.net/v1'
    },
    'chimera': {
        'key': os.environ['CHIMERAGPT_KEY'],
        'url': 'https://chimeragpt.adventblocks.cc/api/v1'
    },
    'api2d': {
        'key': 'fk186035-irVwQKdoHJhfZQN3qKnho2XVdj4rIgaY',
        'url': 'https://stream.api2d.net/v1'
    },
    'shadowgpt': {
        'key': os.environ['GPT4_KEY'],
        'url': 'https://shadowwrite.com/v1'
    },
    'conv': {
        'key': os.environ['CONV_KEY'],
        'url': 'https://infrence-conversion.accswtch50.repl.co/v1'
    },
    'pierangelo': {
        'key': os.environ['PIERWAVEGPT_KEY'],
        'url': 'https://api.pierangelo.info/v1'
    },
    'openai': {
        'key': os.environ['OPENAI_KEY'],
        'url': 'https://api.openai.com/v1'
    },
    'cattogpt': {
        'key': os.environ['CATTO_KEY'],
        'url': 'https://free.catto.codes/v1'
    },
    'purgpt_beta': {
        'key': os.environ['PURGPT_KEY'],
        'url': 'https://beta.purgpt.xyz/openai'
    },
    'purgpt_stable': {
        'key': os.environ['PURGPT_KEY'],
        'url': 'https://purgpt.xyz/openai'
    },
    'purgpt_anthropic': {
        'key': os.environ['PURGPT_KEY'],
        'url': 'https://beta.purgpt.xyz/anthropic'
    },
    'cyclone': {
        'key': os.environ['CYCLONE_KEY'],
        'url': 'https://gpt.darkcoder15.tk/v1'
    },
    'geniusai': {
        'key': dummy_key,
        'url': 'https://geniusai-q9g2.onrender.com/v1'
    },
    'foxgpt': {
        'key': os.environ['FOXGPT_KEY'],
        'url': 'https://api.hypere.app/v1'
    },
    'novaoss': {
        'key': os.environ['NOVAOSS_KEY'],
        'url': 'https://api.nova-oss.com/v1'
    },
    'pawanai': {
        'key': os.environ['PAWAN_KEY'],
        'url': 'https://api.pawan.krd/pai-001-beta/v1',
        'iplock': True
    },
    'pawanai_light': {
        'key': os.environ['PAWAN_KEY'],
        'url': 'https://api.pawan.krd/pai-001-light-beta/v1',
        'iplock': True
    },
    'webraft-openai': {
        'key': os.environ['WEBRAFT_KEY'],
        'url': 'https://api.webraft.in/v1'
    },
    'webraft': {
        'key': os.environ['WEBRAFT_KEY'],
        'url': 'https://thirdparty.webraft.in/v1'
    },
    'zukijourney': {
        'key': os.environ['ZUKI_KEY'],
        'url': 'https://zukijourney.xyzbot.net/v1',
        'iplock': True
    },
    'mikumikumi': {
        'key': os.environ['MIKUMI_KEY'],
        'url': 'https://chat.mikumikumi.tk/v1'
    },
    'galaxyai': {
        'key': os.environ['GALAXY_KEY'],
        'url': 'https://galaxyai.onrender.com/v1'
    },
    'twlgrnexp_anthropic': {
        'key': os.environ['TWLGRNEXP_KEY'],
        'url': 'https://discordexperiment.replit.app',
        'type': 'anthropic'
    },
    'twlgrnexp': {
        'key': os.environ['TWLGRNEXP_KEY'],
        'url': 'https://discordexperiment.replit.app/v1',
        'type': 'openai'
    }
}

def model_params_from_id(model, param):
    for model_data in models_data:
        if model_data["id"] == model:
            return model_data[param]
    return None

def model_category(model):
    return model_params_from_id(model, "category")
    
def check_chat_model(model):
    return model_params_from_id(model, "real_chat")

category_aliases = {
    'chatgpt_3.5': 'novaoss',
    'chatgpt_3.5_old': 'novaoss',
    'gpt_4': 'novaoss',
    'gpt_4_old': 'novaoss',
    'gpt_4_32k': 'novaoss',
    'gpt_4_32k_old': 'novaoss',
    'gpt_3.5_td3': 'webraft',
    "gpt_3.5": 'webraft',
    "gpt_3": 'webraft',
    'claude': 'twlgrnexp_anthropic',
    'claude_obscure': 'twlgrnexp_anthropic',
    'claude-instant-poe': 'claude-instant',
    'claude-2-100k-poe': 'claude-2-100k',
    'claude-instant-100k-poe': 'claude-instant-100k',
    'claude-2-cyclone': 'claude-2'
}

external_category_aliases = config_data["category_aliases"]

category_aliases.update(external_category_aliases)

def iplock_proxy(iplock):
    if iplock:
        os.environ['HTTP_PROXY'] = config_data["proxy"]["http"]
        os.environ['HTTPS_PROXY'] = config_data["proxy"]["https"]
    else:
        if "HTTP_PROXY" in os.environ:
            del os.environ['HTTP_PROXY']
        if "HTTPS_PROXY" in os.environ:
            del os.environ['HTTPS_PROXY']

def gpt_response(model, streaming, messages, temperature, category, iplocked):
    iplock_proxy(iplocked)
    print("Calling API...", flush=True)
    chat_mode = check_chat_model(model)
    if chat_mode:
        if iplocked:
            with noverif.no_ssl_verification():
                return openai.ChatCompletion.create(model=model, stream=streaming, messages=messages, temperature=temperature)
        else:
            return openai.ChatCompletion.create(model=model, stream=streaming, messages=messages, temperature=temperature)
    else:
        non_chat_models_input = {"model": model, "messages": messages,}
        
        prompt = 'This is the description of the AI Assistant "{}". The following is a conversation with said AI assistant. \n\n'.format(non_chat_models_input["messages"][0]["content"])
        for message in non_chat_models_input["messages"][1:]:
            role = "Human" if message["role"] == "user" else "AI"
            prompt += "{}: {}\n".format(role, message["content"])
        prompt += '\n'
        prompt += 'AI: '
        
        if category == 'gpt_3':
            max_tokens = 1024
        else:
            max_tokens = 2048
        
        if iplocked:
            with noverif.no_ssl_verification():
                return openai.Completion.create(model=non_chat_models_input["model"], prompt=prompt, max_tokens=max_tokens, stream=streaming, stop=["Human:", " AI:"], temperature=temperature)
        else:
            return openai.Completion.create(model=non_chat_models_input["model"], prompt=prompt, max_tokens=max_tokens, stream=streaming, stop=["Human:", " AI:"], temperature=temperature)

def conv_response(model, streaming, messages, temperature, iplocked, provider, key=None,):
    iplock_proxy(iplocked)
    if iplocked:
        if streaming:
            with noverif.no_ssl_verification():
                response = conv_module.get_response(model=model, messages=messages, temperature=temperature, streaming=streaming, provider=provider, special=key)
                for token in response:
                    yield {'choices': {0: {'delta': {'content': token}}}}
        else:
            with noverif.no_ssl_verification():
                response = conv_module.get_response(model=model, messages=messages, temperature=temperature, streaming=streaming, provider=provider, special=key)
                return {'choices': {0: {'message': {'content': response}}}}
    else:
        if streaming:
            response = conv_module.get_response(model=model, messages=messages, temperature=temperature, streaming=streaming, provider=provider, special=key)
            for token in response:
                yield {'choices': {0: {'delta': {'content': token}}}}
        else:
            response = conv_module.get_response(model=model, messages=messages, temperature=temperature, streaming=streaming, provider=provider, special=key)
            return {'choices': {0: {'message': {'content': response}}}}

def anthropic_response(model, messages, streaming, temperature, iplocked, max_tokens=30000):
    role_map = {
        "system": HUMAN_PROMPT,
        "user": HUMAN_PROMPT,
        "assistant": AI_PROMPT,
    }
    
    
    prompt = ""
    for message in messages:
        role = message["role"]
        content = message["content"]
        transformed_role = role_map[role]
        prompt += f"{transformed_role} {content}"
    prompt += AI_PROMPT
    iplock_proxy(iplocked)
    if iplocked:
        with noverif.no_ssl_verification():
            completion = anthropic.completions.create(model=model, max_tokens_to_sample=max_tokens, prompt=prompt, temperature=temperature, stream=streaming)
            if streaming:
                for token in response:
                    yield {'choices': {0: {'delta': {'content': token.completion}}}}
            else:
                return {'choices': {0: {'message': {'content': completion.completion}}}}
    else:
        completion = anthropic.completions.create(model=model, max_tokens_to_sample=max_tokens, prompt=prompt, temperature=temperature, stream=streaming)
        if streaming:
            for token in response:
                yield {'choices': {0: {'delta': {'content': token.completion}}}}
        else:
            return {'choices': {0: {'message': {'content': completion.completion}}}}

allowed_unauthorized = ['/backend/models']

@app.before_request
def check_authorization():
    if not request.path in allowed_unauthorized:
        if 'Authorization' not in request.headers:
            abort(401)  # Unauthorized
        # Retrieve the token from the Authorization header
        auth_header = request.headers.get('Authorization')
        #print(auth_header)
        token = auth_header.split('Bearer ')[1]  # Extract the token from "Bearer <token>"
    
        key_array = [os.environ['SELF_KEY']]
        
        if not token in key_array:
            abort(403)  # Forbidden

@app.route("/v1/models", methods=['GET'])
def models_list():
    data = {
        "data": models_data,
        "object": "list"
    }
    return jsonify(data)

@app.route("/backend/models", methods=['GET'])
def model_list():
    provider = request.args.get('provider')
    api_url = api_info[provider]['url']
    api_key = api_info[provider]['key']
    iplock = api_info[provider].get('iplock', False)
    type = api_info[provider].get('type', 'openai')
    if type != 'openai':
        abort(400)
    openai.api_base = api_url
    openai.api_key = api_key
    if iplock:
        os.environ['HTTP_PROXY'] = config_data["proxy"]["http"]
        os.environ['HTTPS_PROXY'] = config_data["proxy"]["https"]
        with noverif.no_ssl_verification():
            return openai.Model.list()
    else:
        if "HTTP_PROXY" in os.environ:
            del os.environ['HTTP_PROXY']
        if "HTTPS_PROXY" in os.environ:
            del os.environ['HTTPS_PROXY']
        return openai.Model.list()

@app.route("/v1/chat/completions", methods=['POST'])
def chat_completions():
    streaming = request.json.get('stream', False)
    model = request.json.get('model', 'gpt-3.5-turbo')
    messages = request.json.get('messages')
    temperature = request.json.get('temperature')
    model_stream = streaming and model_params_from_id(model, 'streaming')
    
    models = {
        #'gpt-3.5-turbo-0613': 'gpt-3.5-turbo',
        #'gpt-3.5-turbo-16k-0613': 'gpt-3.5-turbo-16k',
        #'gpt-4-0613': 'gpt-4',
        'gpt-4-32k-0613': 'gpt-4-32k',
        'bing-purgpt': 'bing',
        'chat-bison-001': 'palm-2'
    }
    
    if model in models:
        used_model = models[model]
    else:
        used_model = model
    
    provider = model_category(model)
    category = provider
    if provider in category_aliases:
        provider = category_aliases[provider]
    api_url = api_info[provider]['url']
    api_key = api_info[provider]['key']
    iplock = api_info[provider].get('iplock', False)
    call_type = api_info[provider].get('type', 'openai')
    global anthropic 
    anthropic = Anthropic(base_url=api_url, api_key=api_key)
    openai.api_base = api_url
    openai.api_key = api_key
    if call_type == 'conv':
        response = conv_response(used_model, model_stream, messages, temperature, iplock, api_url, api_key)
    elif call_type == 'anthropic':
        response = anthropic_response(used_model, messages, model_stream, temperature, iplock)
    else:
        response = gpt_response(used_model, model_stream, messages, temperature, category, iplock)
    """response = post(url = api_url,
                headers = {'Authorization': 'Bearer %s' % api_key},
                json = {
                    'model' : models[model],
                    'messages' : messages,
                    'stream' : streaming
                    },
                stream = streaming
                )
    """
           

    if not streaming:
        while 'curl_cffi.requests.errors.RequestsError' in response:
            response = gpt_response(used_model, model_stream, messages, temperature, category, iplock)

        completion_timestamp = int(time.time())
        completion_id = ''.join(random.choices(
            'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', k=28))
            
        chat_mode = check_chat_model(model)
        if chat_mode:
            message_response = response['choices'][0]['message']['content']
        else:
            message_response = response['choices'][0]['text']

        return json.dumps({
            'id': 'chatcmpl-%s' % completion_id,
            'object': 'chat.completion',
            'created': completion_timestamp,
            'model': model,
            'usage': {
                'prompt_tokens': None,
                'completion_tokens': None,
                'total_tokens': None
            },
            'choices': [{
                'message': {
                    'role': 'assistant',
                    'content': message_response
                },
                'finish_reason': 'stop',
                'index': 0
            }]
        })

    def stream():
        
        #int_response = response.data
        #message_response = json.dumps(int_response)['choices'][0]['delta']['content']
        
        token = ''
        for event in response:
            chat_mode = check_chat_model(model)
            if model_stream:
                if chat_mode:
                    event_text = event['choices'][0]['delta']
                    token = event_text.get('content', '')
                else:
                    event_text = event['choices'][0]
                    token = event_text.get('text', '')
            else:
                if chat_mode:
                    token = response['choices'][0]['message']['content']
                else:
                    token = response['choices'][0]['text']
            if token:
                completion_timestamp = int(time.time())
                completion_id = ''.join(random.choices(
                    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', k=28))

                completion_data = {
                    'id': f'chatcmpl-{completion_id}',
                    'object': 'chat.completion.chunk',
                    'created': completion_timestamp,
                    'model': model,
                    'choices': [
                        {
                            'delta': {
                                'content': token
                            },
                            'index': 0,
                            'finish_reason': None
                        }
                    ]
                }

                yield 'data: %s\n\n' % json.dumps(completion_data, separators=(',' ':'))
                time.sleep(0.01)
                if not model_stream:
                    break
        yield 'data: [DONE]'
        
        
    return app.response_class(stream(), mimetype='text/event-stream')


if __name__ == '__main__':
    config = {
        'host': '0.0.0.0',
        'port': 1337,
        'debug': True
    }

    app.run(**config)
